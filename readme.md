# 猜数字游戏



# 知识点总结

## 定时器

定时器的实现有两种 ，一种是直接用QTimer类，一种是使用`QObject::startTimer()`;

```c++
/*第一种：QTimer类
常用：
start 开始计时
stop 停止计时器
timeout信号和自定义槽函数
*/ 
// 首先创建一个QTimer类的对象t
QTimer* t = new QTimer(this);

// 然后关联信号槽
connect(t, &QTimer::timeout, this, &myWidget::onTimeout);

// 在需要计时的地方，启动定时器
// 启动之后，每秒中会自动触发timeout信号，自定义一个onTimerout槽函数来处理信号
// void QTimer::start(std::chrono::milliseconds msec) 毫秒
t->start(1000);//启动计时器
t->stop();//停止计时器

```

```c++
/* 第二种：计时器事件
常用：
startTime(int) 启动计时器
killTimer(int) 停止计时器
timerEvent 重写计时器事件
*/

// QObecjt类有个startTimer的公开方法
// startTimer会返回该计时器的id
int startTimer(std::chrono::milliseconds time, Qt::TimerType timerType = Qt::CoarseTimer)

startTimer(1000);
/*
开始计时之后，每隔1秒都会触发计时器事件(timer event)；
需要重写一个timeEvent()函数，处理这个计时器事件，该事件的id可以从startTimer的返回值得到。
*/

// 示例
// 启动定时器，返回给overTimerId
overTimerId = startTimer(5000); 

//重写timerEvent函数
void myWidget::timerEvent(QTimerEvent* e)
{
    if (e->timerId() == overTimerId) {//overTimerId的计时事件
			//该事件要做什么
        }
    } else if (/* 另一个事件 */) {
		//该事件要做什么
    }
}

//停止计时器
void QObject::killTimer(int id);
//停止overTimerId事件
killTimer(overTimerId);
```



## 类型转换qobject_cast

相当于dynamic_cast<>()转换，

```c++
//统一处理数字按键时用到
// sender会返回一个QObject对象指针，表示是谁发出的这个信号
// 从QObject转换为QPushButton
QPushButton* p = qobject_cast<QPushButton*>(sender());
```



## QMovie类

用来显示没有声音的动画，比如gif，如果需要有声音，需要用Qt Multimedia模块

```c++
//用法
QLabel label;

// 创建一个QMovie对象，指定动画的路径，一般是资源文件的路径
QMovie *movie = new QMovie("animations/fire.gif");

// 载体上设置该动画
label.setMovie(movie);

// 启动动画
movie->start();

//停止动画
movie->stop();

```





## 随机数生成

```c++
// 用法与c语言的随机数生成类似
// 生成随机数种子
qsrand(QTime(0, 0, 0).secsTo(QTime::currentTime()));
// 生成随机数0-99
num = qrand() % 100;
```

```c++
// 示例
```



## 动态进度条progressBar的实现

```c++
// 创建计时器
m_timer = new QTimer(this);
connect(m_timer, &QTimer::timeout, this, &myWidget::onTimeout);
// 开始计时
m_timer->start(1000);

// 成员属性
m_num = 10;
// 设置progressBar的最大值、最小值、开始的值
ui->progressBar->setMinimum(0);
ui->progressBar->setMaximum(10);
ui->progressBar->setValue(m_num);

/*
计时器开始后，每秒都重复触发timeout信号，
在槽函数onTimeout中处理该信号
*/
qDebug() << "调用了onTimeut";
m_num--;//每秒减1
ui->progressBar->setValue(m_num);//更新进度条的值
if (m_num == 0) {//直到减到0之后，停止计时
    qDebug() << "停止计时";
    m_timer->stop();
}
//这样动态的进度条就弄出来了
```

## 界面设计stackWidget

之前不知道那些在一个窗口内切换不同的页面是怎么做的，我以为是又新建了一个新的窗口，没想到还可以**直接添加stackWidget的页面，每一个page页面代表不同的界面**。这样切换功能的时候，主窗口的大小是不变的，所以看起来更为顺畅。



