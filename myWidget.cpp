#include "myWidget.h"
#include "ui_myWidget.h"
#include <QMessageBox>
#include <QTime>
myWidget::myWidget(QWidget* parent)
    : QWidget(parent)
    , ui(new Ui::myWidget)
{
    ui->setupUi(this);
    this->setWindowTitle("猜数字游戏");
    this->setWindowIcon(QIcon(":/images/Image/game.svg"));
    ui->stackedWidget->setCurrentWidget(ui->pageSet);

    // 失败动画
    overMovie.setFileName(":/images/Image/over.gif");
    ui->labelOver->setMovie(&overMovie); // 设置动画
    ui->labelOver->setScaledContents(true); // 自适应大小
    // 胜利动画
    winMovie.setFileName(":/images/Image/win.gif");
    ui->labelWin->setMovie(&winMovie); // 设置动画
    ui->labelWin->setScaledContents(true); // 自适应大小

    connect(ui->btn0, &QPushButton::clicked, this, &myWidget::dealNum);
    connect(ui->btn1, &QPushButton::clicked, this, &myWidget::dealNum);
    connect(ui->btn2, &QPushButton::clicked, this, &myWidget::dealNum);
    connect(ui->btn3, &QPushButton::clicked, this, &myWidget::dealNum);
    connect(ui->btn4, &QPushButton::clicked, this, &myWidget::dealNum);
    connect(ui->btn5, &QPushButton::clicked, this, &myWidget::dealNum);
    connect(ui->btn6, &QPushButton::clicked, this, &myWidget::dealNum);
    connect(ui->btn7, &QPushButton::clicked, this, &myWidget::dealNum);
    connect(ui->btn8, &QPushButton::clicked, this, &myWidget::dealNum);
    connect(ui->btn9, &QPushButton::clicked, this, &myWidget::dealNum);
}

myWidget::~myWidget()
{
    delete ui;
}

void myWidget::dealNum()
{
    QPushButton* p = qobject_cast<QPushButton*>(sender());
    if (p != NULL) {
        QString numStr = p->text();
        resultStr += numStr;

        // 数字不能从0开始
        if (resultStr.size() == 1 && resultStr == "0") {
            resultStr.clear();
        }

        if (resultStr.size() <= 4) {
            ui->textEdit->setText(resultStr);
            if (resultStr.size() == 4) {
                if (resultStr > randStr) {
                    ui->textEdit->append("数字大了点!!!");

                } else if (resultStr < randStr) {
                    ui->textEdit->append("数字小了点!!!");
                } else {
                    ui->textEdit->append("恭喜你猜对了");

                    killTimer(gameTimerId);
                    QMessageBox::information(this, "胜利", "恭喜你猜对了");

                    // 切换成功动画
                    winMovie.start();
                    ui->stackedWidget->setCurrentWidget(ui->pageWin);

                    // 启动定时器
                    winTimerId = startTimer(5000);
                }
                // 清空
                resultStr.clear();
            }
        }
    }
}

void myWidget::on_btnStart_clicked()
{
    // 获取下拉框的时候
    gameTime = ui->comboBox->currentText().toInt();
    // 切换到游戏界面
    ui->stackedWidget->setCurrentWidget(ui->pageGame);

    int num;
    qsrand(QTime(0, 0, 0).secsTo(QTime::currentTime()));
    while ((num = qrand() % 10000) < 999) {
    }
    randStr = QString::number(num);

    // 设置进度条
    ui->progressBar->setMinimum(0); // 最小值
    ui->progressBar->setMaximum(gameTime); // 最大值
    ui->progressBar->setValue(gameTime); // 当前值

    gameTimerId = 0;
    gameTimerId = startTimer(1000);
    resultStr.clear();
    ui->textEdit->clear();
}

void myWidget::on_btnEnd_clicked()
{
    this->close();
}

void myWidget::timerEvent(QTimerEvent* e)
{
    if (e->timerId() == gameTimerId) {
        gameTime--;
        // 设置进度条
        ui->progressBar->setValue(gameTime); // 设置当前值
        if (gameTime == 0) {
            // 关闭定时器
            killTimer(gameTimerId);
            QMessageBox::information(this, "失败", "时间到了");

            overMovie.start(); // 启动动画

            ui->stackedWidget->setCurrentWidget(ui->pageOver);

            overTimerId = startTimer(5000); // 启动结束动画定时器
        }
    } else if (e->timerId() == overTimerId) {
        overMovie.stop(); // 停止动画
        killTimer(overTimerId);
        ui->stackedWidget->setCurrentWidget(ui->pageSet);

    } else if (e->timerId() == winTimerId) {
        winMovie.stop();
        killTimer(winTimerId);
        ui->stackedWidget->setCurrentWidget(ui->pageSet);
    }
}

void myWidget::on_btnDel_clicked()
{
    if (resultStr.size() == 1) {
        resultStr.clear();
        ui->textEdit->clear();
    } else {
        resultStr.chop(1);
        ui->textEdit->setText(resultStr);
    }
}

void myWidget::on_btnTips_clicked()
{
    resultStr.clear();
    QString str = "随机数为" + randStr;
    ui->textEdit->setText(str);
}
