#ifndef MYWIDGET_H
#define MYWIDGET_H
#include <QMovie>
#include <QWidget>

QT_BEGIN_NAMESPACE
namespace Ui {
class myWidget;
}
QT_END_NAMESPACE

class myWidget : public QWidget {
    Q_OBJECT

public:
    myWidget(QWidget* parent = nullptr);
    ~myWidget();
    void dealNum();

private slots:
    void on_btnStart_clicked();
    void on_btnEnd_clicked();
    void on_btnDel_clicked();
    void on_btnTips_clicked();

protected:
    void timerEvent(QTimerEvent* e);

private:
    Ui::myWidget* ui;
    QMovie overMovie;
    QMovie winMovie;

    QString resultStr; // 结果数
    int gameTimerId;
    int overTimerId;
    int winTimerId;

    QString randStr;

    int gameTime;
};
#endif // MYWIDGET_H
